// Lab_9.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"
#include <mutex>

#define MyCreateThread(func, parametr) CreateThread(0, 0, func, parametr, 0, 0);
#define DEBUG

std::mutex mutex;
DWORD threadCount;

DWORD producerFileIndex = 0;
DWORD consumerFileIndex = 0;
TCHAR *fileNames[];

DWORD WINAPI task23ThreadFunc(PVOID parametr)
{
	FILE** fHandle = new FILE*[1];
	HANDLE hMutex = CreateMutex(0, false, _T("myMutex"));
	OpenMutex(MUTEX_ALL_ACCESS, false, _T("myMutex"));
	WaitForSingleObject(hMutex, INFINITE);

	_tprintf(_T("Started %d%s\n"), parametr, _T(" thread."));
#ifdef DEBUG
	fopen_s(fHandle, "d:\\threads\\log.txt", "a");

	if(fHandle == NULL)
	{
		return false; 
	}

	fprintf(fHandle[0], "Started %d%s\n", parametr, " thread.");
#endif // DEBUG
	
	int number = 0;
	for(int i = 0; i < 100000000; i++)
	{
		number++;
	}

	_tprintf(_T("%d%s\n"), parametr, _T(" thread has finished."));

#ifdef DEBUG
	fprintf(fHandle[0], "%d%s\n", parametr, " thread has finished.");

	fclose(fHandle[0]);
#endif // DEBUG

	ReleaseMutex(hMutex);

	return 0;
}

DWORD WINAPI task4ThreadFunc(void*)
{
	mutex.lock();
	threadCount++;
	mutex.unlock();
	return 0;
}

DWORD WINAPI anotherTask4ThreadFunc(void*)
{
	mutex.lock();

	threadCount++;

	mutex.unlock();

	Sleep(INFINITE);

	_tprintf(_T("Thread finished."));

	return 0;
}

bool Task23()
{
	HANDLE *h = new HANDLE[10];
	bool result = false;

#ifdef DEBUG
	FILE** fHandle = new FILE*[1];
	fopen_s(fHandle, "d:\\threads\\log.txt", "w");
	fclose(fHandle[0]);
#endif // DEBUG

	for(int i = 0; i < 10; i++)
	{
		h[i] = MyCreateThread(task23ThreadFunc, (LPVOID)((DWORD) i));
		result |= (bool)h[i];
	}

	WaitForMultipleObjects(10, h, true, INFINITE);
	_tprintf(_T("All processes have been finished."));

#ifdef DEBUG
	fopen_s(fHandle, "d:\\threads\\log.txt", "a");
	fprintf(fHandle[0], "All processes have been finished.");
	fclose(fHandle[0]);
#endif // DEBUG

	return result;
}

bool Task3()
{
#ifdef DEBUG
	FILE** fHandle = new FILE*[1];
	fopen_s(fHandle, "d:\\threads\\log.txt", "w");
	fclose(fHandle[0]);
#endif

	for(int i = 0; i < 10; i++)
	{
		STARTUPINFO si = {0};
		PROCESS_INFORMATION pi;	
		TCHAR comLine[MAX_PATH];

		_stprintf_s(comLine, _T("ChildProcess.exe %d"), (DWORD)i);
		BOOL b;
		b = CreateProcess(0, comLine, 0, 0, 0, 0, 0, 0, &si, &pi);
	}

	return true;
}

bool Task4()
{
	DWORD maxThreads = 5;
	threadCount = 0;
	HANDLE *hThreads = new HANDLE[maxThreads];

	while(true)
	{
		hThreads = new HANDLE[maxThreads];
		threadCount = 0;

		// Create threads.
		mutex.lock();

		for(int i = 0; i < maxThreads; i++)
		{
			//hThreads[i] = CreateThread(NULL, 0, task4ThreadFunc, NULL, 0, 0);
			hThreads[i] = MyCreateThread(task4ThreadFunc, 0);
		}
		
		mutex.unlock();

		// Threads are now able to proceed.
		WaitForMultipleObjects(maxThreads, hThreads, true, INFINITE);
		// All the proccesses have finished.
		_tprintf(_T("%d\n"), threadCount);

		for(int i = 0; i < maxThreads; i++)
		{
			CloseHandle(hThreads[i]);
		}

		if(threadCount != maxThreads)
		{
			// Found the number of threads.
			_tprintf(_T("END\n"));
			break;
		}
		maxThreads++;
	}

	return false;
}

bool anotherTask4()
{
	threadCount = 0;
	HANDLE *hThreads = new HANDLE[20000];
	BOOL b = true;
	int i = 0;

	for(; i < 20000; i++)
	{
		hThreads[i] = MyCreateThread(anotherTask4ThreadFunc, 0);
		WaitForSingleObject(hThreads[i], 40);

		if(i + 1 != threadCount)
			break;
	}

	_tprintf(_T("I = %d\n"), i);
	_tprintf(_T("ThreadCount = %d\n"), threadCount);

	return false;
}

bool Task5()
{
	TCHAR* cSemaphore = _T("consumerSemafore");
	TCHAR* pSemaphore = _T("producerSemafore");
	TCHAR* qMutex = _T("queueMutex");
	TCHAR* mapName = _T("myMap");
	TCHAR* counterMapName = _T("myCounterMap");
	DWORD queueLength = 10;

	STARTUPINFO si = {sizeof(STARTUPINFO)};
	PROCESS_INFORMATION pi;

	HANDLE producerSemaphore = CreateSemaphore(0, 10, 10, pSemaphore);
	HANDLE consumerSemaphore = CreateSemaphore(0, 0, 10, cSemaphore);
	HANDLE queueMutex = CreateMutex(0, FALSE, qMutex);

	HANDLE hCounter = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, sizeof(DWORD), counterMapName);
	if((hCounter == INVALID_HANDLE_VALUE) || (hCounter == NULL))
	{
		_tprintf(_T("Error creating mapping (%d).\n"), GetLastError());
	}
	HANDLE h = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, 8000, mapName);
	if((h == INVALID_HANDLE_VALUE) || (h == NULL))
	{
		_tprintf(_T("Error creating mapping (%d).\n"), GetLastError());
	}

	TCHAR comLineConsumer[MAX_PATH];
	TCHAR comLineProducer[MAX_PATH];

	_stprintf_s(comLineProducer, _T("Producer.exe %s %s %s %s %s"), pSemaphore, cSemaphore, mapName, qMutex, counterMapName);
	_stprintf_s(comLineConsumer, _T("Consumer.exe %s %s %s %s"), pSemaphore, cSemaphore, mapName, qMutex);
	

	BOOL b = CreateProcess(0, comLineConsumer, 0, 0, 0, 0, 0, 0, &si, &pi);
	if(!b)
	{
		_tprintf(_T("Error creating consumer process.\n"));
	}
	//b = CreateProcess(0, comLineConsumer, 0, 0, 0, 0, 0, 0, &si, &pi);
	//b = CreateProcess(0, comLineConsumer, 0, 0, 0, 0, 0, 0, &si, &pi);


	b = CreateProcess(0, comLineProducer, 0, 0, 0, 0, 0, 0, &si, &pi);
	if(!b)
	{
		_tprintf(_T("Error creating producer process.\n"));
	}

	/*b = CreateProcess(0, comLineConsumer, 0, 0, 0, 0, 0, 0, &si, &pi);
	if(!b)
	{
		_tprintf(_T("Error creating consumer process.\n"));
	}*/

	return 1;
}

int _tmain(int argc, _TCHAR* argv[])
{
	//Task23();
	//Task3();
	//anotherTask4();
	Task5();


	getchar();

	return 0;
}

