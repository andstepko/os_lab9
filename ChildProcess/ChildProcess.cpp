// ChildProcess.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"

#define DEBUG

int _tmain(int argc, _TCHAR* argv[])
{
	FILE** fHandle = new FILE*[1];
	HANDLE hMutex = CreateMutex(0, false, _T("myMutex"));
	OpenMutex(MUTEX_ALL_ACCESS, false, _T("myMutex"));
	WaitForSingleObject(hMutex, INFINITE);

	_tprintf(_T("Started %s%s\n"), argv[1], _T(" thread."));
#ifdef DEBUG
	fopen_s(fHandle, "d:\\threads\\log.txt", "a");

	if(fHandle == NULL)
	{
		return false; 
	}

	fprintf(fHandle[0], "Started %s%s\n", argv[1], " thread.");
#endif // DEBUG
	
	int number = 0;
	for(int i = 0; i < 100000000; i++)
	{
		number++;
	}

	_tprintf(_T("%s%s\n"), argv[1], _T(" thread has finished."));

#ifdef DEBUG
	fprintf(fHandle[0], "%s%s\n", argv[1], " thread has finished.");

	fclose(fHandle[0]);
#endif // DEBUG

	ReleaseMutex(hMutex);

	return 0;
}