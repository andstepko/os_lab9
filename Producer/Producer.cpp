// Producer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"


int _tmain(int argc, _TCHAR* argv[])
{
	TCHAR* pSemaphore = argv[1];
	TCHAR* cSemaphore = argv[2];
	TCHAR* mapName = argv[3];
	TCHAR* qMutex = argv[4];
	TCHAR* counterMapName = argv[5];

	LPVOID pBuf;
	LPVOID pCounterBuf;
	BYTE *bCurMem;
	BYTE *bCounterCurMem;

	DWORD count;
	DWORD dwWritten;

	while(1)
	{
		HANDLE producerSemaphore = OpenSemaphore(SEMAPHORE_ALL_ACCESS, FALSE, pSemaphore);
		HANDLE consumerSemaphore = OpenSemaphore(SEMAPHORE_ALL_ACCESS, FALSE, cSemaphore);
		WaitForSingleObject(producerSemaphore, INFINITE);
		_tprintf(_T("Hey, I'm Producer!.\n"));

		//HANDLE hQueueMutex = CreateMutex(0, false, qMutex);
		HANDLE hQueueMutex = OpenMutex(MUTEX_ALL_ACCESS, FALSE, qMutex);
		//_tprintf(_T("Queue mutex handle _produecer_ == (%d).\n"), hQueueMutex);
		DWORD result = WaitForSingleObject(hQueueMutex, INFINITE);
		//_tprintf(_T("Result of waiting of queueHandle == (%d).\n"), result);
		// Queue of files is free, let's occupy :D.
		OpenMutex(MUTEX_ALL_ACCESS, false, qMutex);

		// Counter mapping.
		HANDLE hCounterMapFile = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, counterMapName);
		if (hCounterMapFile == NULL)
		{
			_tprintf(_T("Could not open file counterMapping object (%d).\n"), GetLastError());
			return 1;
		}
		pCounterBuf = MapViewOfFile(hCounterMapFile,   // handle to map object
							FILE_MAP_ALL_ACCESS, // read/write permission
							0,
							0,
							sizeof(DWORD));
		if (pCounterBuf == NULL)
		{
			_tprintf(_T("Could not map view of counter file (%d).\n"), GetLastError());
			CloseHandle(hCounterMapFile);
			return 1;
		}
		bCounterCurMem = (BYTE*) pCounterBuf;
		*(DWORD*)(bCounterCurMem) += 1;
		count = *(DWORD*)(bCounterCurMem);



		if(count == 5) return 1;



		//_tprintf(_T("Counter mapping = (%d).\n"), *(DWORD*)(bCounterCurMem));

		TCHAR fileName[10];
		_stprintf_s(fileName, _T("file%d"), count);
		//_tprintf(_T("FileName == (%s).\n"), fileName);
		// End of counter mapping.

		// Queue mapping.
		HANDLE hMapFile = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, mapName);
		if (hMapFile == NULL)
		{
			_tprintf(_T("Could not open file mapping object (%d).\n"), GetLastError());
			return 1;
		}
		pBuf = MapViewOfFile(hMapFile,   // handle to map object
							FILE_MAP_ALL_ACCESS, // read/write permission
							0,
							0,
							8000);
		if (pBuf == NULL)
		{
			_tprintf(_T("Could not map view of file (%d).\n"), GetLastError());
			CloseHandle(hMapFile);
			return 1;
		}

		bCurMem = (BYTE*) pBuf;
		DWORD pointer = 0;

		/*_tprintf(_T(" pointer == (%d), char == (%d).\n"), 0, bCurMem[0]);
		_tprintf(_T(" pointer == (%d), char == (%d).\n"), 1, bCurMem[1]);
		_tprintf(_T(" pointer == (%d), char == (%d).\n"), 2, bCurMem[2]);
		_tprintf(_T(" pointer == (%d), char == (%d).\n"), 3, bCurMem[3]);
		_tprintf(_T(" pointer == (%d), char == (%d).\n"), 4, bCurMem[4]);
		_tprintf(_T(" pointer == (%d), char == (%d).\n"), 5, bCurMem[5]);
		_tprintf(_T(" pointer == (%d), char == (%d).\n"), 6, bCurMem[6]);
		_tprintf(_T(" pointer == (%d), char == (%d).\n"), 7, bCurMem[7]);
		_tprintf(_T(" pointer == (%d), char == (%d).\n"), 8, bCurMem[8]);
		_tprintf(_T(" pointer == (%d), char == (%d).\n"), 9, bCurMem[9]);
		_tprintf(_T(" pointer == (%d), char == (%d).\n"), 10, bCurMem[10]);
		_tprintf(_T(" pointer == (%d), char == (%d).\n"), 11, bCurMem[11]);
		_tprintf(_T(" pointer == (%d), char == (%d).\n"), 12, bCurMem[12]);
		_tprintf(_T(" pointer == (%d), char == (%d).\n"), 13, bCurMem[13]);
		_tprintf(_T(" pointer == (%d), char == (%d).\n"), 14, bCurMem[14]);
		_tprintf(_T(" pointer == (%d), char == (%d).\n"), 15, bCurMem[15]);
		_tprintf(_T(" pointer == (%d), char == (%d).\n"), 16, bCurMem[16]);
		_tprintf(_T(" pointer == (%d), char == (%d).\n"), 17, bCurMem[17]);
		_tprintf(_T(" pointer == (%d), char == (%d).\n"), 18, bCurMem[18]);
		_tprintf(_T(" pointer == (%d), char == (%d).\n"), 19, bCurMem[19]);
		_tprintf(_T(" pointer == (%d), char == (%d).\n"), 20, bCurMem[20]);
		_tprintf(_T(" pointer == (%d), char == (%d).\n"), 21, bCurMem[21]);
		_tprintf(_T(" pointer == (%d), char == (%d).\n"), 22, bCurMem[22]);
		_tprintf(_T(" pointer == (%d), char == (%d).\n"), 23, bCurMem[23]);
		_tprintf(_T(" pointer == (%d), char == (%d).\n"), 24, bCurMem[24]);
		_tprintf(_T(" pointer == (%d), char == (%d).\n"), 25, bCurMem[25]);*/

		//_tprintf(_T("Just before while.\n"));
		while(bCurMem[pointer] != '\0')
		{
			pointer += sizeof(TCHAR);
		}
		//pointer++;
		bCurMem += pointer;
		//_tprintf(_T("Pointer == (%d).\n"), pointer);
		TCHAR fileNameItem[11];
		_stprintf_s(fileNameItem, _T("%s\n"), fileName);
		_tcscpy_s((TCHAR*)bCurMem, _tcslen(fileNameItem) * sizeof(TCHAR), fileNameItem);
		
		//_tprintf(_T("bCurMem:: (%s).\n"), (TCHAR*)(BYTE*) pBuf);
		// Print the queue.
		_tprintf((TCHAR*)(BYTE*) pBuf);
		_tprintf(_T("\n"));


		// File creation.
		TCHAR fullPath[100] = _T("d:\\threads\\pc\\");
		_tcscat_s(fullPath, fileName);
		_tcscat_s(fullPath, _T(".txt"));

		//_tprintf(_T("fullPath:: (%s)\n"), fullPath);
		HANDLE hFile = CreateFile((LPCTSTR)fullPath, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, CREATE_ALWAYS, 0, 0);
		if((hFile == INVALID_HANDLE_VALUE) || (hFile == NULL))
		{
			_tprintf(_T("Error creating file (%d), fullPath:: (%s).\n"), GetLastError(), fullPath);
			return 1;
		}
		//_tprintf(_T("File was successfully created.\n"));
		//_tprintf(_T("hFile == (%d).\n"), hFile);
		WriteFile(hFile, (LPCVOID)fileName, _tcslen(fileName) * sizeof(TCHAR), &dwWritten, 0);
		if(!CloseHandle(hFile))
		{
			_tprintf(_T("Error closing file handle.\n"));
		}

		UnmapViewOfFile(pCounterBuf);
		CloseHandle(hCounterMapFile);
		UnmapViewOfFile(pBuf);
		CloseHandle(hMapFile);

		//_tprintf(_T("End of producer syncronised block.\n"));

		// Free the queue of files.
		DWORD result2 = ReleaseMutex(hQueueMutex);
		//_tprintf(_T("Result of releasing queue mutex:: (%d)\n"), result2);
		if(result2 == 0)
		{
			_tprintf(_T("Error of releasing queue mutex:: (%d)\n"), GetLastError());
		}
		// Allow one more consumer to do its job.
		ReleaseSemaphore(consumerSemaphore, 1, 0);		

		DWORD toSleep = 5000;
		//_tprintf(_T("Producer is falling asleep.. (%d)\n"), toSleep);
		Sleep(toSleep);
	} // while

	return 0;
}