// Consumer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"


int _tmain(int argc, _TCHAR* argv[])
{
	TCHAR* pSemaphore = argv[1];
	TCHAR* cSemaphore = argv[2];
	TCHAR* mapName = argv[3];
	TCHAR* qMutex = argv[4];
	DWORD dwRead;
	DWORD dwKcuf;

	LPVOID pBuf;
	BYTE *bCurMem;

	while(1)
	{
		HANDLE producerSemaphore = OpenSemaphore(SEMAPHORE_ALL_ACCESS, FALSE, pSemaphore);
		HANDLE consumerSemaphore = OpenSemaphore(SEMAPHORE_ALL_ACCESS, FALSE, cSemaphore);

		DWORD result = WaitForSingleObject(consumerSemaphore, INFINITE);
		//_tprintf(_T("Hello, I'm Consumer!!!\n"));
		_tprintf(_T("Hello, I'm Consumer!!!    "));

		//HANDLE hQueueMutex = CreateMutex(0, false, qMutex);
		HANDLE hQueueMutex = OpenMutex(MUTEX_ALL_ACCESS, FALSE, qMutex);
		//_tprintf(_T("Queue mutex handle _consumer_ == (%d).\n"), hQueueMutex);
		WaitForSingleObject(hQueueMutex, INFINITE);
		// Queue of files is free, let's occupy :D.

		// Queue mapping.
		HANDLE hMapFile = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, mapName);
		if (hMapFile == NULL)
		{
			_tprintf(_T("Could not open file mapping object (%d).\n"), GetLastError());
			return 1;
		}
		pBuf = MapViewOfFile(hMapFile, // handle to map object
							FILE_MAP_ALL_ACCESS, // read/write permission
							0,
							0,
							8000);
		if (pBuf == NULL)
		{
			_tprintf(_T("Could not map view of file (%d).\n"), GetLastError());
			CloseHandle(hMapFile);
			return 1;
		}
		//_tprintf(_T("The file was successfully mapped by a consumer.\n"));

		bCurMem = (BYTE*) pBuf;
		TCHAR* tcharMem = (TCHAR*) pBuf;
		TCHAR* tcharCurMem = (TCHAR*) pBuf;
		DWORD pointer = 0;




		TCHAR fileName[MAX_PATH];
		_tcscpy_s(fileName, _tcslen((TCHAR*)bCurMem) * sizeof(TCHAR), (TCHAR*)bCurMem);

		//_tprintf(_T("Before while.\n"));
		while(tcharCurMem[pointer] != '\n')
		{
			fileName[pointer] = tcharCurMem[pointer];
			pointer++;
		}
		fileName[pointer] = '\0';




		//while(bCurMem[pointer] != '\0')
		//{
		//	pointer += sizeof(TCHAR);
		//	//_tprintf(_T("While iteration.\n"));
		//}
		//pointer -= 2 * sizeof(TCHAR);
		//while((pointer > 0) && (bCurMem[pointer] != '\n'))
		//{
		//	pointer -= sizeof(TCHAR);
		//}
		//if(pointer > 0)
		//	pointer += sizeof(TCHAR);
		//// Pointer is now at the start of the fileName.
		////_tprintf(_T("Pointer == (%d).\n"), pointer);
		//_tprintf(_T("Pointer == (%d).\n"), pointer);

		//bCurMem += pointer;
		tcharCurMem += pointer + 1;

		//strcpy_s((char*)bCurMem, 11, "\0\0\0\0\0\0\0\0\0\0\0");

		//_tprintf(_T("\ntcharCurMem == (%s)\n"), tcharCurMem);
		//_tprintf(_T("tcharMem == (%s)\n"), tcharMem);
		//_tprintf(_T("_tcslen(tcharCurMem) == (%d)\n"), _tcslen(tcharCurMem));
		if(_tcslen(tcharCurMem) == 0)
		{
			tcharMem[0] = '\0';
		}
		else
		{
			//_tprintf(_T("WOW!!!\n"));
			_tcscpy_s(tcharMem, _tcslen(tcharCurMem) * sizeof(TCHAR), tcharCurMem);
			//_tcscpy_s(tcharMem, _tcslen(tcharCurMem), _T("kcuf"));
		}

		// Delete the last '\n' symbol.
		//fileName[_tcslen((TCHAR*)bCurMem) - 1] = '\0';
		//_tprintf(_T("fileName:: (%s).\n"), fileName);
		TCHAR fullPath[100] = _T("d:\\threads\\pc\\");
		_tcscat_s(fullPath, fileName);
		_tcscat_s(fullPath, _T(".txt"));
		
		//_tprintf(_T("Got from stack:: (%s).\n"), fullPath);
		// Get file content.
		TCHAR content[MAX_PATH];
		HANDLE hFile = CreateFile((LPCTSTR)fullPath, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
		ReadFile(hFile, &content, GetFileSize(hFile, 0), &dwRead, 0);
		//ReadFile(hFile, &content, 10, &dwRead, 0);
		//_tprintf(_T("dwRead == (%d).\n"), dwRead);
		TCHAR content2[MAX_PATH];
		_tcscpy_s(content2, MAX_PATH * sizeof(TCHAR), content);
		content2[dwRead / sizeof(TCHAR)] = '\0';
		_tprintf(_T("Got from FILE:: (%s).\n"), content2);

		// Print the queue.
		_tprintf((TCHAR*)(BYTE*) pBuf);
		_tprintf(_T("\n"));

		UnmapViewOfFile(pBuf);
		CloseHandle(hMapFile);

		//_tprintf(_T("End of consumer syncronised block.\n"));

		// Allow one more producer to do its job.
		ReleaseSemaphore(producerSemaphore, 1, 0);
		// Free the queue of files.
		ReleaseMutex(hQueueMutex);

		DWORD toSleep = 10000;
		//_tprintf(_T("Consumer is falling asleep.. (%d)\n"), toSleep);
		Sleep(toSleep);
	}

	return 0;
}

